#!/bin/bash

export LC_ALL=C

find "$@" -name '*.pub' -print | sort | while read file ; do
    ssh-keygen -l -f "$file" | cut -d ' ' -f2 | tr -d :
done
