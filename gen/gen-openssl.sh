#!/bin/bash

set -e

if ! test -e "$HOME" ; then
    echo "home directory $HOME does not exist"
    exit 2
fi

type=$1
case "$type" in
    rsa[0-9]*)
	bits=$(echo "$type" | cut --bytes 4-)
	type=rsa
	;;
    dsa)
	bits=
	;;
    ?*)
	echo "key type argument invalid"
	exit 1
	;;
    *)
	echo "key type argument missing"
	exit 1
	;;
esac

arch=$(dpkg-architecture -qDEB_HOST_ARCH)
dso=./setpid-$arch.so

prefix=keys/openssl/$arch/$type$bits
rm -rf $prefix
mkdir -p $prefix/rnd

for x in {1..32767} ; do
    echo "*** PID $x"
    rm -f $HOME/.rnd
    LD_PRELOAD=$dso SETPID=$x openssl gen$type -out $prefix/$x.key $bits
    test -e $HOME/.rnd
    LD_PRELOAD=$dso SETPID=$x openssl gen$type -out $prefix/$x-rnd.key $bits
done
