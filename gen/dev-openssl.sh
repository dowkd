#!/bin/bash

export LC_ALL=C

find "$@" -name '*.key' -print | sort | while read file ; do
    openssl rsa -noout -modulus -in "$file"
done | perl dev-openssl.pl
