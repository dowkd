#!/bin/bash

set -e

type=$1
case "$type" in
    rsa|rsa1|dsa)
	;;
    ?*)
	echo "key type argument invalid"
	exit 1
	;;
    *)
	echo "key type argument missing"
	exit 1
	;;
esac

bits=$2
if test -z "$bits" ; then
    echo "bits argument missing"
    exit 1
fi

arch=$(dpkg-architecture -qDEB_HOST_ARCH)
dso=./setpid-$arch.so

prefix=keys/ssh/$arch/$type/$bits
rm -rf $prefix
mkdir -p $prefix

for x in {1..32767} ; do
    echo "*** PID $x"
    file=$prefix/$x
    LD_PRELOAD=$dso SETPID=$x ssh-keygen -t $type -b $bits -P "" -f $file
done
