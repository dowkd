use strict;
use warnings;

while (my $line = <STDIN>) {
    if ($line =~ /^Modulus=([0-9A-F]+)$/) {
	my $mod = $1;
	$mod = substr $mod, length($mod) - 32;
	$mod =~ y/A-F/a-f/;
	my @mod = $mod =~ /(..)/g;
	print join('', reverse @mod) . "\n";
    } else {
	chomp $line;
	warn "warning: unparsable line: $line\n";
    }
}

__END__
# Old version, uses -text output

while (my $line = <STDIN>) {
    last if $line =~ /^modulus:/;
}

my @bytes;
while (my $line = <STDIN>) {
    last if $line !~ /^\s/;
    chomp $line;
    $line =~ s/^\s+//;
    push @bytes, split m/:/, $line;
}

print join("", (reverse @bytes)[0..15]) . "\n";

