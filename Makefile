BLACKLIST_FILES := data/DSA-1024 data/RSA-2048 \
	data/OpenVPN-64-LE data/OpenVPN-32-LE \
	$(wildcard data/OpenSSH-*-LE) \
	$(wildcard data/OpenSSL-*-LE)

export LC_ALL=C

.PHONY: all force

all: dowkd

dowkd: dowkd.in dowkd.blacklist.gz dowkd.blacklist.md5 ChangeLog force
	-rm $@ dowkd.pl.gz 2> /dev/null || true
	perl -c dowkd.in
	bash dowkd.compile

dowkd.blacklist: $(BLACKLIST_FILES)
	-rm $@ 2> /dev/null || true
	sort -u $(BLACKLIST_FILES) -o $@

dowkd.blacklist.gz: dowkd.blacklist
	gzip -9 < $^ > $@

dowkd.blacklist.md5: dowkd.blacklist
	-rm $@ 2> /dev/null || true
	md5sum $^ | awk '{print $$1}' > $@

# Generate the ChangeLog from the GIT metadata
ChangeLog : force
	-rm $@ 2> /dev/null || true
	git log --pretty=oneline | while read sha text ; do \
	  t=$$(git describe "$$sha" --exact-match --match 'v[0-9]*' 2>/dev/null) ; \
	  test -z "$$t" || echo "$$t" ; \
	  echo "  $$text" ; \
	done > $@

force:
